<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	$title = wp_title( '&raquo;', false, 'right' );
	$title = explode(':', $title);
	echo $title[0];

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'huc' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="icon" href="<?php bloginfo( 'stylesheet_directory' ); ?>/img/favicons/favicon_32px.png" type="image/x-png" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?ver=<?php echo CLINIC_THEME_V; ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or yout will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();

	global $clases, $menu_items;

	//Menu items created with front_page children pages
	$frontpage_id = get_option( 'page_on_front' );

	$args = array(
		'post_parent' => $frontpage_id,
		'post_type'   => 'page',
		'numberposts' => -1,
		'orderby' => 'menu_order'
	);
	$children = new WP_Query( $args );

	$menu_items = array();
	$home_url = (!is_front_page()) ? get_the_permalink($frontpage_id) : "";
	if($children && $children->have_posts()) {
		while($children->have_posts()) {
			$children->the_post();
			if(in_array( str_replace('.php', '', basename( get_page_template() ) ) , array( 'page' ))) {
				if(!is_front_page())
					$menu_items[] = sprintf("<li data-section='seccion-%s'><a href='%s#seccion-%s'>%s</a></li>", $post->post_name, $home_url, $post->post_name, $post->post_title);
				else
					$menu_items[] = sprintf("<li data-section='seccion-%s'><a href='%s#seccion-%s' class='scroll-to' data-section='seccion-%s'>%s</a></li>", $post->post_name, $home_url, $post->post_name, $post->post_name, $post->post_title);
			}
		}
		wp_reset_postdata();
	}

	//Reserve a time page
	$_reserve = get_page_by_path('reserve-a-time');
	$reserve = get_permalink($_reserve);
	$reserve = 'https://www.clockwisemd.com/hospitals/770/appointments/new';
?>
	<script>var reserve_link = '<?php echo $reserve; ?>';</script>
</head>
<body <?php body_class($clases); ?>>
	<div class="container-fluid wrapper">
        <!--MENU-->
            <nav id="nav-menu" class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
					    <button class="reserve reserve-button btn btn-primary btn-justifice btn-red">reserve a time</button>
                        <button class="navbar-toggle link" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="<?php bloginfo('url'); ?>" class="navbar-bran">
                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/icons/ic_logo_white.png" alt="Logo" class="logo logo-white">
                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/icons/ic_logo_red.png" alt="Logo" class="logo logo-red">
                            <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/favicons/favicon_32px.png" alt="Logo" class="logo logo-movil">
                        </a>
                    </div>
                    <div class="collpase navbar-collpase" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <?php echo implode("", $menu_items); ?>
                        </ul>
                    </div>
                </div>
            </nav><!--End menu-->
