<?php
/**
 * The template for displaying the quorum page.
 *
 */

global $clases;

$clases[] = 'background-white';

$background = get_post_meta( $post->ID, '_background', true );
if(has_post_thumbnail() && $background == 'background-opacity-white') $clases[] = 'background-white';

$children = false;
if( !$post->post_parent ) {
	$args = array(
		'post_parent' => $post->ID,
		'post_type'   => 'page', 
		'numberposts' => -1,
		'orderby' => 'menu_order'
	);
	$children = new WP_Query( $args );
}


get_header(); ?>

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<section id="seccion-<?php echo $post->post_name; ?>" class="<?php echo $background; ?>">
					<?php get_template_part( 'content', $post->post_type ); ?>
					</section>
				<?php endwhile; ?>
				
				<?php if($children) : while ( $children->have_posts() ) : $children->the_post(); ?>
					
					<?php 
						$template = str_replace('.php', '', basename( get_page_template() ) ); 
						$background = get_post_meta( $post->ID, '_background', true );
												
						$child_classes = array();
						if($template != 'no-title') $child_classes[] = "wp";
						$child_classes[] = $background;
					?>
					<section id="seccion-<?php echo $post->post_name; ?>" <?php if(count($child_classes) >0) : ?>class="<?php echo implode(" ", $child_classes); ?>"<?php endif; ?>>
					<?php get_template_part( 'content', $template ); ?>
					</section>
				<?php endwhile; endif; ?>

			<?php else : ?>

				<?php get_template_part( 'content', '404' ); ?>

			<?php endif; ?>

<?php get_footer(); ?>
