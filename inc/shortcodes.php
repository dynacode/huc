<?php

add_filter( 'no_texturize_shortcodes', 'bsc_shortcodes_to_exempt_from_wptexturize' );

add_shortcode( 'accordion', 'bsc_accordion' );
add_shortcode( 'accordion_item', 'bsc_accordion_item' );

add_shortcode( 'cloud', 'bsc_cloud' );
add_shortcode( 'slider', 'bsc_slider' );
add_shortcode( 'bsc_ninja_form', 'bsc_ninja_form' );


function bsc_accordion( $atts, $content ) {
	global $accordion_item;
	if(!$accordion_item) $accordion_item = 1;

	$data = shortcode_atts( array(
    'id' => 'acordeon_'.mt_rand(1000,9999),
  ), $atts );

  $content = substr($content, 5);
  $content = substr($content, 0, -4);

  $data['content'] = do_shortcode(shortcode_unautop( $content ));

  return clinic_render( 'shortcode-accordion-group.tpl', $data, false );
}

function bsc_accordion_item( $atts, $content ) {
	global $accordion_item;
	if(!$accordion_item) $accordion_item = 1;

	$data = shortcode_atts( array(
      'title' => 'Item '.$accordion_item,
  ), $atts );
  $accordion_item++;

	$data['content'] = $content;

  return clinic_render('shortcode-accordion-item.tpl', $data, false);
}

function bsc_cloud( $atts, $content ) {
	$data = shortcode_atts( array(
    'tax' => 'category',
		'classes' => '',
  ), $atts );

	//Classes
  $data['classes'] = trim($data['classes'] . ' list-group');

	//LIst of terms
  $terms = get_terms( $data['tax'], array(
		'hide_empty' => false,
	) );

	$items = array();
	foreach($terms as $term) {
		//If not an issue or has been declared to be displayed
		if($data['tax'] != 'issue' || get_term_meta( $term->term_id, 'display', true ))
			$items[] = $term->name;
	}
	$data['items'] = $items;

  return clinic_render( 'shortcode-cloud.tpl', $data, false );
}

function bsc_slider( $atts ) {
	global $post;
	$data = shortcode_atts( array(
		'id' => 'slider_'.mt_rand(1000,9999),
		'post-type' => 'post',
        'classes' => false,
        'view-items' => 1,
        'time' => 8000,
        'titles' => false,
        'type' => 'carrusel',
        'not_in' => false,
				'max' => 50
    ), $atts
	);

	//Set max
	$data['max'] = (int)$data['max'];

	//Set if titles are required
	$data['titles'] = ($data['titles'] == 'true') ? true : false;

	//Search for items
  $args = array(
		'post_type' => $data['post-type'],
		'posts_per_page' => -1,
  );

	if($data['not_in']) {
		$not_in = explode(",", $data['not_in']);
		$args['post__not_in'] = $not_in;
  }

  $items = new WP_Query( $args );

	//Set type
  if(!in_array($data['type'], array( 'roundabout' ))) $data['type'] = 'carrusel';

  $listado = "";
  $titulos = false;
  $count = 1;
  if( $items->have_posts() ) {
		while( $items->have_posts() ) {
			$items->the_post();

			switch( $post->post_type ) {
				case "service":
					//issues
					$_issues = wp_get_object_terms( $post->ID, 'issue', array( 'fields' => 'all' ) );

					//Get only a set of issues
					$total = count($_issues);
					$_issues = array_slice($_issues, 0, $data['max']);

					//Get the prices of the issues
					$issues = array();
					foreach( $_issues as $issue ) {
						$issue_price = get_term_meta( $issue->term_id, 'price', true );
						$issues[] = array(
							'title' => $issue->name,
							'price' => empty($issue_price) ? false : $issue_price,
						);
          }

					//If we have a subset
					if(count($issues) > 0 ) {
						if($total > count($issues)) {
							$issues[] = array(
									'title' => "And Much, More...",
									'price' => false,
							);
						}
					}

					//Get data
					$content = apply_filters('the_content', $post->post_content);
					$price = get_post_meta($post->ID, '_price', true);

					$listado[] = array(
						'title' => $post->post_title,
						'content' => apply_filters('the_content', $post->post_content),
						'type' => $post->post_type,
						'issues' => $issues,
						'price' => empty($price) ? false : $price,
					);

					break;
				default:
					$listado[] = array(
						'title' => $post->post_title,
						'content' => apply_filters('the_content', $post->post_content),
						'type' => $post->post_type,
					);
			}
			if($data['titles']) $titulos[$count] = $post->post_title;
			$count++;
		}

		wp_reset_postdata();
	}

	$data['titulos'] = $titulos;
	$data['listado'] = $listado;

	return do_shortcode( clinic_render( 'shortcode-slider.tpl', $data, false ) );
}

function bsc_shortcodes_to_exempt_from_wptexturize( $shortcodes ) {
    //$shortcodes[] = 'row';
    //$shortcodes[] = 'content-text';
    //$shortcodes[] = 'accordion';
    //$shortcodes[] = 'accordion_item';
    return $shortcodes;
}

?>
