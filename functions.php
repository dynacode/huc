<?php
/**
 * Hollywood Urgent Care
 *
 */

define("CLINIC_THEME_V", "0.1.3");

require('inc/shortcodes.php');

add_action( 'init', 'clinic_init' );
add_action('wp_head', 'clinic_header');
add_action( 'after_setup_theme', 'clinic_setup' );
add_action('customize_register','clinic_customize_theme');

add_action( 'add_meta_boxes', 'clinic_boxes_themes' );

add_action( 'save_post_page', 'clinic_save_page_extra' );

//add_filter( 'excerpt_length', 'clinic_multi_excerpt_length', 1000 );
//add_filter('wp_trim_excerpt', 'clinic_new_excerpt_more');

//show_admin_bar( false );


function clinic_header() {
	wp_register_script('resizeEnd', get_bloginfo('stylesheet_directory').'/js/jQuery.resizeEnd.js', array('jquery'), CLINIC_THEME_V);
	wp_register_script('waypoints', get_bloginfo('stylesheet_directory').'/js/jquery.waypoints.min.js', array('jquery'), CLINIC_THEME_V);
	wp_register_script('modernizr', get_bloginfo('stylesheet_directory').'/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', array('jquery'), CLINIC_THEME_V);
	wp_register_script('bootstrap', get_bloginfo('stylesheet_directory').'/js/vendor/bootstrap.min.js', array('jquery'), CLINIC_THEME_V);
	wp_register_script('caroufredcel', get_bloginfo('stylesheet_directory').'/js/caroufredcel/jquery.carouFredSel.js', array('jquery'), CLINIC_THEME_V);
	wp_register_script('carrusel', get_bloginfo('stylesheet_directory').'/js/caroufredcel/carrusel.js', array('caroufredcel'), CLINIC_THEME_V);
	wp_register_script('clinica', get_bloginfo('stylesheet_directory').'/js/main.js', array('jquery', 'carrusel', 'jquery-ui-accordion', 'bootstrap', 'modernizr', 'waypoints', 'resizeEnd'), CLINIC_THEME_V);
	wp_enqueue_script('clinica');

	wp_register_style('bootstrap', get_bloginfo('stylesheet_directory').'/css/bootstrap.min.css', array(), CLINIC_THEME_V);
	wp_register_style('bootstrap-theme', get_bloginfo('stylesheet_directory').'/css/bootstrap-theme.min.css', array(), CLINIC_THEME_V);
	wp_register_style('jquery-ui', get_bloginfo('stylesheet_directory').'/css/jquery-ui.css', array(), CLINIC_THEME_V);
	wp_register_style('clinica', get_bloginfo('stylesheet_directory').'/css/main.css', array('bootstrap', 'bootstrap-theme', 'jquery-ui'), CLINIC_THEME_V);
	wp_register_style('icomoon', get_bloginfo('stylesheet_directory').'/css/icomoon.css', array(), CLINIC_THEME_V);
	wp_register_style('responsive', get_bloginfo('stylesheet_directory').'/css/responsive.css', array(), CLINIC_THEME_V);
	wp_print_styles( array( 'clinica', 'icomoon', 'responsive' ));

}

function clinic_init() {
	add_post_type_support( 'page', 'thumbnail' );
}

function clinic_setup() {

	// Add default posts and comments RSS feed links to <head>.
	//add_theme_support( 'automatic-feed-links' );

	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'menu', 'Menú' );

	// The sidebar
	//register_sidebar(array('name' => 'Barra'));

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be the size of the header image that we just defined
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	//set_post_thumbnail_size( 160, 120, true );

	// Tamaños de imágenes
	//add_image_size( 'abstract', 130, 130, true );
}

//Declare boxes
function clinic_boxes_themes() {
	global $post;

	//Boz for pages
	add_meta_box(
        'page_extra',
        'Page data',
        'clinic_box_page_extra',
        'page',
        'side'
    );
}

// Box for pages extra data
function clinic_box_page_extra() {
	global $post;

	// Obtener data
	$background = get_post_meta($post->ID,'_background',true);

	//Background types
	$bg_types = array(
		array ('id' => 'none', 'name' => 'None'),
		array ('id' => 'gray', 'name' => 'Gray'),
		array ('id' => 'light-gray', 'name' => 'Light gray'),
		array ('id' => 'dark-gray', 'name' => 'Dark Gray'),
		array ('id' => 'background-opacity-white', 'name' => 'White opacity'),
		array ('id' => 'background-opacity', 'name' => 'Black opacity')
	);

	$args= array(
		'background' => $background,
		'bg_types' => $bg_types,
	);

	clinic_render("box-template_page_extra.tpl", $args);

}

// Function to save page extra data
function clinic_save_page_extra( $post_id ) {
	// Return if this is an autosave routine
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	// Return if this isn't an edit action
	if(!isset($_POST['action']) || $_POST['action']!= 'editpost')
		return;

	// Check if user can edit the post
	if ( !current_user_can( 'edit_post', $post_id ) ) return;

	// Get data
	$background = ($_POST['background'] == 'none') ? false : $_POST['background'];

	// Almacenar variables
	update_post_meta($post_id, '_background', $background);

	return;
}

function clinic_customize_theme( $wp_customize ) {
	//Facebook
	$wp_customize->add_setting(
		'clinic_facebook',
		array(
			'type' => 'theme_mod', // or 'option'
			'sanitize_callback' => 'esc_url',
		)
	);
	$wp_customize->add_control(
		'clinic_facebook',
		array(
			'type' => 'text',
			'label' => 'Facebook',
			'description' => __('Facebook page URL', 'huc'),
			'section' => 'clinic_redes'
		)
	);

	//Twitter
	$wp_customize->add_setting(
		'clinic_twitter',
		array(
			'type' => 'theme_mod', // or 'option'
			'sanitize_callback' => 'esc_url',
		)
	);
	$wp_customize->add_control(
		'clinic_twitter',
		array(
			'type' => 'text',
			'label' => 'Twitter',
			'description' => __('Twitter page URL', 'huc'),
			'section' => 'clinic_redes'
		)
	);

	//Linkedin
	$wp_customize->add_setting(
		'clinic_linkedin',
		array(
			'type' => 'theme_mod', // or 'option'
			'sanitize_callback' => 'esc_url',
		)
	);
	$wp_customize->add_control(
		'clinic_linkedin',
		array(
			'type' => 'text',
			'label' => 'Linkedin',
			'description' => __('Linkedin page URL', 'huc'),
			'section' => 'clinic_redes'
		)
	);

	//Yelp
	$wp_customize->add_setting(
		'clinic_yelp',
		array(
			'type' => 'theme_mod', // or 'option'
			'sanitize_callback' => 'esc_url',
		)
	);
	$wp_customize->add_control(
		'clinic_yelp',
		array(
			'type' => 'text',
			'label' => 'Yelp',
			'description' => __('Yelp page URL', 'huc'),
			'section' => 'clinic_redes'
		)
	);

	//Sección de redes
	$wp_customize->add_section(
		'clinic_redes',
		array(
			'title' => __('Social networks', 'huc'),
		)
	);
}

function clinic_render( $tpl, $args, $echo = true ) {
	$context = Timber::get_context();
	//$context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );
	Timber::$dirname = 'templates';

	//Render
	if($echo) return Timber::render($tpl, array_merge($context, $args));
	// o feed?
	return Timber::fetch($tpl, array_merge($context, $args));

}

?>
