<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$page_classes = array();
$page_classes[] = 'row';

if(has_post_thumbnail()) {
	$image_id = get_post_thumbnail_id();
	$image = wp_get_attachment_image_src( $image_id, 'large' );
	$imagen = $image[0];
}

$background = get_post_meta($post->ID, '_background', true);
if($background == 'background-opacity') $page_classes[] = "background-opacity";
if($background == 'background-opacity-white') $page_classes[] = "background-opacity background-opacity-white";
 
?>

	<div id="page-<?php the_ID(); ?>" class="grid-row background-img" <?php if($imagen): ?>style="background-image: url('<?php echo $imagen; ?>');"<?php endif; ?>>
		<?php if(count($page_classes)>0) : ?><div class="<?php echo implode(" ", $page_classes); ?>"><?php endif; ?>
			<?php the_content(); ?> 
		<?php if(count($page_classes)>0) : ?></div><?php endif; ?>
	</div><!-- #page-<?php the_ID(); ?> -->
