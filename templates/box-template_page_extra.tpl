<div>
    <label>Background:</label>
    <select name="background">
        {% for item in bg_types %}
            <option value="{{ item.id }}" {{ function('selected', item.id, background, false) }}>{{ item.name }}</option>
        {% endfor %}
    </select>
</div>
