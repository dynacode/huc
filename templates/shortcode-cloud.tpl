<div class='content-list'>
  <ul class='{{ classes }}'>
    {% for item in items %}
    <li class='list-group-item'>{{ item }}</li>
    {% endfor %}
  </ul>
</div>
