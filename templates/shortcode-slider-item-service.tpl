<div class='item'>
  <h1>{{ item.title }}</h1>
  {% if item.price %}<h1>{{ item.price }}</h1>{% endif %}
  <div>
    <h1>Includes such issues as:</h1>
  </div>
  <div class='item-list light-gray'>
    {% if item.issues %}<ul>
      {% for issue in item.issues %}
        <li>{{ issue.title }}{% if issue.price %} {{ issue.price }}{% endif %}</li>
      {% endfor %}
    </ul>{% endif %}
  </div>
</div>
