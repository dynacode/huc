<div id='{{ id }}' class='{{ type }}{{ clases }}' data-view-items='{{ view-items }}' data-items-by-width='{{ view-items }},{{ view-items }},1,1' data-time='{{ time }}'>
    {% if titulos and titles %}
    <div class='carousel filter'>
        <ul class='nav navbar-nav uppercase white'>
            {% for key, item in titulos %}
            <li><a data-go={{ key }}>{{ item }}</a></li>
            {% endfor %}
        </ul>
    </div>{% endif %}
    <!--Items slider-->
    <div class='listado '>
        {% for item in listado %} {% include ['shortcode-slider-item-'~item.type~'.tpl', 'shortcode-slider-item.tpl'] %} {% endfor %}
    </div>
    <!-- Flechas desplazamiento-->
    <a href='#{{id}}' class='carousel-control left button-prev' data-slider='prev'>
        <span class='glyphicon glyphicon-arrow-left'></span>
    </a>
    <a href='#{{id}}' class='carousel-control right button-next' data-slider='next'>
        <span class='glyphicon glyphicon-arrow-right'></span>
    </a>
</div>