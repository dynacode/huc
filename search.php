<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 */

global $wp_query;
get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

			<h1>Palabra clave: <?php echo get_search_query(); ?></h1>

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'abstract', $post->post_type ); ?>

				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'content', '404' ); ?>

			<?php endif; ?>

			</div><!-- #content -->
			<?php wp_pageNavi(); ?>
		</div><!-- #primary -->

<?php get_footer(); ?>
