//Función para atender carruseles
function carrusel(element) {
	jQuery(element).find('.listado').trigger('destroy', true);
	var w_width = jQuery(window).width();
	
	var _widths = jQuery(element).attr("data-items-by-width");
	if (typeof _widths !== typeof undefined && _widths !== false) {
		var widths = _widths.split(","); 
		jQuery(element).attr('data-view-items', widths[0]);
		if(w_width < 1010) {
			jQuery(element).attr('data-view-items', widths[1]);
		}
		if(w_width < 830) {
			jQuery(element).attr('data-view-items', widths[2]);
		}
		if(w_width < 630) {
			jQuery(element).attr('data-view-items', widths[3]);
		}
	}
	
	//Tamaños base
	var time = parseInt(jQuery(element).attr('data-time'));
	if(time === undefined) time = 4000;
	var scroll_items = jQuery(element).attr('data-scroll-items');
	if(scroll_items === undefined) scroll_items = 1;
	var view_items = jQuery(element).attr('data-view-items');
	if(view_items === undefined) view_items = 1;
	var width = jQuery(element).attr('data-width');
	if(width === undefined) width = 'null';
	var margin = jQuery(element).attr('data-margin');
	if(margin === undefined) margin = 0;
	var fx = jQuery(element).attr('data-fx');
	if(fx === undefined) fx = 'left';
	var filtro = jQuery(element).attr('data-filtro');
	if(filtro === undefined) filtro = false;
	
	if(jQuery(element).hasClass('roundabout')) width = Math.min(720,w_width - 30);
			
	//Responsive
	var responsive = false;
	if(jQuery(element).attr('id') == 'banner') responsive = true;

	var scroll = {
		items			: Number(scroll_items),
		duration		: 800,
		fx				: fx,
		pauseOnHover	: true,
	}; 
	
	if(jQuery(window).innerWidth() < 400 || Number(view_items) > 1) {
		scroll['fx']= 'swipe';
	}
	
	if(filtro) items = {
		visible			: Number(view_items),
		filter			: '.'+filtro
	};
	
	var car_width = jQuery(element).width() - Number(margin);
	jQuery(element).find('.listado > div').css("width", (car_width / Number(view_items))+"px");
		
	jQuery(element).find('.listado').carouFredSel({
		width				: width,
		items				: Number(view_items),
		scroll 				: scroll,
		responsive			: responsive,
		height				: 'auto',
		auto : {
			play			: true,
			items			: Number(scroll_items),
			timeoutDuration	: time
		},
		next				: jQuery(element).find('.button-next'),
		prev				: jQuery(element).find('.button-prev'),
		pagination : {
			container		: jQuery(element).find('.paginador'),
			anchorBuilder	: function(nr, item) {
				return "<span class='item'>" + nr +"</span>";
			}
		},
		
	});
	
	if(width != 'null') {
		jQuery(element).find('.listado').parent().css("margin", "auto");
	}
}

function roundabout(element) {
	var time = parseInt(jQuery(element).attr('data-time'));
	if(time === undefined) time = 8000;
	
	var height = jQuery(element).find('.listado .item .item-list').height() + 250;
	
	var _center = {
		width: 500,
		//height: 700,
		fontSize: '1em',
		//transform: 'scale(1)',
		marginLeft: 0,
		marginTop: 0,
		marginRight: 0,
		opacity: 1,
	};
	var _left = {
		width: 300,
		//height: 600,
		fontSize: '0.8em',
		//transform: 'scale(0.8)',
		marginLeft: 0,
		marginTop: 50,
		marginRight: -100,
		opacity: 0.3,
	};
	var _right = {
		width: 300,
		//height: 600,
		fontSize: '0.8em',
		//transform: 'scale(0.8)',
		marginLeft: -100,
		marginTop: 50,
		marginRight: 0,
		opacity: 0.3,
	};
	var _outLeft = {
		width: 300,
		//height: 600,
		fontSize: '0.8em',
		marginLeft: 200,
		marginTop: 50,
		marginRight: -400,
		opacity: 0.3,
	};
	var _outRight = {
		width: 300,
		//height: 600,
		fontSize: '0.8em',
		marginLeft: 200,
		marginTop: 50,
		marginRight: -400,
		opacity: 0.3,
	};
	jQuery(element).find('.listado').carouFredSel({
		width: 900,
		height: height,
		align: false,
		items: {
			visible: 3,
			width: 100
		},
		auto : {
			play			: true,
			timeoutDuration	: time
		},
		scroll: {
			items: 1,
			duration: 400,
			onBefore: function( data ) {
				if(data.scroll.direction == "next") {
					data.items.old.eq( 0 ).animate(_outLeft);
					data.items.visible.eq( 0 ).animate(_left);
					data.items.visible.eq( 1 ).animate(_center);
					data.items.visible.eq( 2 ).animate(_right).css({ zIndex: 1 });
					data.items.visible.eq( 2 ).next().css(_outRight).css({ zIndex: 0 });
	
					setTimeout(function() {
						data.items.old.eq( 0 ).css({ zIndex: 1 });
						data.items.visible.eq( 0 ).css({ zIndex: 2 });
						data.items.visible.eq( 1 ).css({ zIndex: 3 });
						data.items.visible.eq( 2 ).css({ zIndex: 2 });
					}, 200);
				} else {					
					data.items.old.eq( 2 ).animate(_outRight);
					data.items.visible.eq( 2 ).animate(_right);
					data.items.visible.eq( 1 ).animate(_center);
					data.items.visible.eq( 0 ).animate(_left).css({ zIndex: 1 });
					data.items.visible.eq( -1 ).css(_outLeft).css({ zIndex: 0 });
	
					setTimeout(function() {
						data.items.old.eq( 2 ).css({ zIndex: 1 });
						data.items.visible.eq( 2 ).css({ zIndex: 2 });
						data.items.visible.eq( 1 ).css({ zIndex: 3 });
						data.items.visible.eq( 0 ).css({ zIndex: 2 });
					}, 200);
				}
			}
		},
		next				: jQuery(element).find('.button-next'),
		prev				: jQuery(element).find('.button-prev'),		
	});
	jQuery(element).find('.listado').children().eq( 0 ).css(_left).css({ zIndex: 2 });
	jQuery(element).find('.listado').children().eq( 1 ).css(_center).css({ zIndex: 3 });
	jQuery(element).find('.listado').children().eq( 2 ).css(_right).css({ zIndex: 2 });
	jQuery(element).find('.listado').children().eq( 3 ).css(_outRight).css({ zIndex: 1 });
}

