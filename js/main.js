var scroll_top = 0;

jQuery(document).ready(function() {
	
	//scroll_top += jQuery( 'div.logo_container' ).height();
	//scroll_top += jQuery( '#redes_sociales' ).height();
	if(jQuery('body').hasClass('admin-bar')) scroll_top += jQuery( '#nav-menu' ).height();
	
	/*Acordion*/
	var icons = {
	  header: "ui-icon-triangle-1-s",
	  activeHeader: "ui-icon-triangle-1-s"
	};
	
	jQuery( ".accordion" ).accordion({
		icons: "icons",
		heightStyle: "content"
	});
	
	jQuery('.link[data-link]').on('click', function(event) {
		event.stopPropagation();
		window.location.href = jQuery(this).attr('data-link');
	});
	
	jQuery('.reserve-button').on('click', function(event) {
		window.open('https://clockwisemd.com/hospitals/770/appointments/new');
	});
	
	/*Tama�o Menu fijo scroll*/
	jQuery(window).scroll(function() {
		if ( jQuery(this).scrollTop() > scroll_top && !jQuery('#nav-menu').hasClass("navbar-fijo")) {
			var top = 0;
			if(jQuery('body').hasClass('admin-bar')) top += jQuery( '#wpadminbar' ).height();
			jQuery('#nav-menu').addClass("navbar-fijo").css({position:"fixed", top: top+"px"}); //.animate({fontSize:"0.7em"}, 200); 
			scroll_top = 0;
			//scroll_top += jQuery( 'div.logo_container' ).height();
			//scroll_top += jQuery( '#redes_sociales' ).height();
			if(jQuery('body').hasClass('admin-bar')) scroll_top += jQuery( '#wpadminbar' ).height();
		} else if ( jQuery(this).scrollTop() <= scroll_top && jQuery('#nav-menu').hasClass("navbar-fijo")) {
			jQuery('#nav-menu').removeClass("navbar-fijo").css({position:"absolute", top: "initial"}); //.animate({fontSize:"1em"}, 200);
			scroll_top = 0;
			//scroll_top += jQuery( 'div.logo_container' ).height();
			//scroll_top += jQuery( '#redes_sociales' ).height();
			if(jQuery('body').hasClass('admin-bar')) scroll_top += jQuery( '#wpadminbar' ).height();
		}
	});
	
	/*Scroll to section menu*/
	jQuery('.scroll-to').on('click', function( event ) {
		var id = jQuery(this).attr('data-section');
		jQuery('html, body').animate({
			scrollTop: jQuery("#" + id).offset().top
		}, 2000);
		return false;
	});
	
	/*Waypoints*/
	jQuery('section.wp').each( function( index ) {
		jQuery(this).waypoint( function( direction ) {
				jQuery('#nav-menu .navbar-nav li').removeClass('selected');
				var mark =  '#nav-menu .navbar-nav li[data-section='+ this.element.id +']';
				if(direction === 'down' ) jQuery( mark ).addClass('selected');
				else jQuery( mark ).prev().addClass('selected');
			}, {
			  offset: 20
			}
		);
	});
	
	/*Slider*/
	jQuery(".carrusel").each( function(index) { carrusel(this); });
	jQuery(window).resizeend(function(){ 
		jQuery(".carrusel").each( function(index) { carrusel(this); });
	});
	
	/*Roundabout*/
	
 	var w_width = jQuery(window).width(); 
	if(w_width > 1010) {
		jQuery(".roundabout").each( function(index) { 
			equalheight(jQuery(this).find('.listado .item .item-list'));
			roundabout(this); 
		});
	} else {
		jQuery(".roundabout").each( function(index) { carrusel(this); });
	}
	
	jQuery(window).resizeend(function(){ 
		var w_width = jQuery(window).width(); 
		if(w_width > 1010) {
			jQuery(".roundabout").each( function(index) { 
				equalheight(jQuery(this).find('.listado .item .item-list'));
				roundabout(this); 
			});
		} else {
			jQuery(".roundabout .listado .item").removeAttr( 'style' );
			jQuery(".roundabout").each( function(index) { carrusel(this); });
		}
	});
	
 	/* Roundabout filter */
	jQuery('.roundabout .carousel.filter a').on('click', function() {
		var w_width = jQuery(window).width(); 
		var pos = jQuery(this).attr('data-go');
		var $parent = jQuery(this).parent().parent().parent().parent();
		var id = $parent.attr('id');
		var currentPosition = jQuery('#'+id+' .listado').triggerHandler("currentPosition");
		var moveTo = pos - 2;
		var desplazar = moveTo - currentPosition;
		var totalItems = jQuery('#'+id+' .listado .item').length;				
		var desplazar2 = totalItems + desplazar;
		
		if(Math.abs(desplazar2) < Math.abs(desplazar)) desplazar = desplazar2;
		
		if(w_width < 1010) desplazar++;
		
		if(desplazar < 0) {
			roundaboutPrev(id, desplazar);
		}
		if(desplazar > 0) {
			roundaboutNext(id, desplazar);
		}
		//jQuery('#'+id+' .listado').trigger('slideTo', parseInt(pos));
	});
	
});

function roundaboutPrev( id, desplazar ) {
	if(desplazar == 0) return;
	desplazar++;
	jQuery('#'+id+' .listado').trigger('prev', function() { roundaboutPrev( id, desplazar ) });
}

function roundaboutNext( id, desplazar ) {
	if(desplazar == 0) return;
	desplazar--;
	jQuery('#'+id+' .listado').trigger('next', function() { roundaboutNext( id, desplazar ) });
}

//Function to set divs with equal height
function equalheight(container) {

	var currentTallest = 0;
	jQuery(container).each(function() {
	
		$el = jQuery(this);		
		if ($el.height() > currentTallest) {
			currentTallest = $el.height();
		}
	});
	
	jQuery(container).each(function() {
			jQuery(this).height(currentTallest);
	});
}
