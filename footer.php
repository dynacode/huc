<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

 global $menu_items;

 $privacy = get_page_by_path('privacy');
 $legal = get_page_by_path('terms-and-conditions');

 $twitter = get_theme_mod( 'clinic_twitter', false );
 $facebook = get_theme_mod( 'clinic_facebook', false );
 $linkedin = get_theme_mod( 'clinic_linkedin', false );
 $yelp = get_theme_mod( 'clinic_yelp', false );

?>

<footer>
                <div class="footer-widgets white gray ">
                    <div class="footer-logo">
                        <a href="home.html" ><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/icons/ic_logo_red.png" alt="Logo" class="logo"></a>
                    </div>
                    <div class="container">
                        <div class="col-sm-3 col-xs-12">
                        <div class="widget widget-col-1">
                            <ul>
                                <li>6430 Selma Avenue.</li>
                                <li>Hollywood,</li>
                                <li>California 90028</li>

                            </ul>
                            <button class="btn btn-primary btn-justifice btn-red reserve-button">reserve a time </button>
                        </div>

                        </div>

                        <div class="col-sm-3 col-xs-12">
                            <div class="widget ">
                                <h3 class="white uppercase">call us at</h3>
                                <ul>
                                    <li>323.848.4522</li>
                                </ul>
                            </div>

                            <div class="widget">
                                <h3 class="white uppercase">email us at</h3>
                                <ul>
                                    <li>info@hollywoodclinic.net</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-3 col-xs-12">
                            <div class="widget">
                                <h3 class="white uppercase">follow us at</h3>
                                <ul class="redes">
                                    <?php if($facebook) : ?><li><a target="_BLANK" href="<?php echo $facebook; ?>"> <span class="icon icon-facebook"></span>  Facebook</a></li><?php endif; ?>
                                    <?php if($twitter) : ?><li><a target="_BLANK" href="<?php echo $twitter; ?>"> <span class="icon icon-twitter"></span>  Twitter</a></li><?php endif; ?>
                                    <?php if($linkedin) : ?><li><a target="_BLANK" href="<?php echo $linkedin; ?>"> <span class="icon icon-linkedin2"></span>  LinkedIn</a></li><?php endif; ?>
                                    <?php if($yelp) : ?><li><a target="_BLANK" href="<?php echo $yelp; ?>"> <span class="icon icon-yelp"></span>  Yelp</a></li><?php endif; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div class="widget">
                                <h3 class="white uppercase">discover</h3>
                                <ul class="nav nav-footer">
                                     <?php echo implode("", $menu_items); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-bottom gray-font dark-gray">
                    <div class="info">
                        <div class="col-xs-7 col-sm-7 ">
                            <h4 class="uppercase">hours of operation:</h4>
                            <div class="content-title">
                                <h3 class="uppercase">monday - friday: 8am - 8pm</h3>
                                <h3 class="uppercase">saturday: 9am - 4pm</h3>
                                <h3 class="uppercase">sunday: 9am - 3pm</h3>

                            </div>
                            <p>We may occasionally accept our last patient prior to closing times listed based on patient volume and wait time</p>
                        </div>
                    </div>
                    <div class="copy">
                        <p>Copyright 2016 - All rigths reserved</p>
                        <div class="info">
                            <ul >
                                <li><a href="<?php echo get_the_permalink($legal); ?>">Terms and conditions</a></li>
                                <li><a href="<?php echo get_the_permalink($privacy); ?>">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div><!--End container-fluid-->

        <?php wp_footer(); ?>
    </body>
</html>
