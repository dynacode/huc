<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 */

global $clases;

$clases[] = 'background-white';

get_header(); ?>

				<?php get_template_part( 'content', '404' ); ?>

<?php get_footer(); ?>
