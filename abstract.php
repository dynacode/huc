<?php
    global $post;
?>
						<div class="abstract">
							<h1><a href='<?php the_permalink(); ?>'><?php the_title(); ?></a></h1>
							<?php if (has_post_thumbnail()) {
    the_post_thumbnail('abstract');
} ?>
							<?php the_excerpt(); ?>
						</div>
