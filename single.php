<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 */
 
global $clases;

$clases[] = 'background-white';

get_header(); ?>

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', $post->post_type ); ?>
					
					</section>
				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'content', '404' ); ?>

			<?php endif; ?>

<?php get_footer(); ?>
